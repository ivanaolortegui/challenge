import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerBtnComponent } from './container-btn.component';

describe('ContainerBtnComponent', () => {
  let component: ContainerBtnComponent;
  let fixture: ComponentFixture<ContainerBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
