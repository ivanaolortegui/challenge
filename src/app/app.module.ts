import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBtnComponent } from './components/nav-btn/nav-btn.component';
import { HeaderComponent } from './components/header/header.component';
import { ContainerBtnComponent } from './components/container-btn/container-btn.component';
import { PageOneComponent } from './pages/page-one/page-one.component';
import { PageTwoComponent } from './pages/page-two/page-two.component';
import { TabComponent } from './components/tab/tab.component';
import { CategoryComponent } from './components/category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBtnComponent,
    HeaderComponent,
    ContainerBtnComponent,
    PageOneComponent,
    PageTwoComponent,
    TabComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
